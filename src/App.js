import Home from './Screen/MainScreen/Home';
import Navbar from './Component/Navbar';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import JasaProg from './Screen/Jasa/JasaProg';
import JasaGraph from './Screen/Jasa/JasaGraph';
import JasaVideo from './Screen/Jasa/JasaVideo';
import DetailJasa from './Screen/Jasa/DetailJasa';
import CreateJasa from './Screen/Dashboard/CreateJasa';
import Jasa from './Screen/MainScreen/Jasa';
import Project from './Screen/Dashboard/Project'
import AboutUs from './Screen/MainScreen/AboutUs';
import NavbarDash from './Component/NavbarDash';
import Posting from './Screen/Dashboard/Posting';
import CreateProject from './Screen/Dashboard/CreateProject';

function App() {
  return (
    <Router>
      <div className="App">
        <Switch>
          <Route exact path="/">
            <Navbar />
            <Home />
          </Route>
          <Route exact path="/jasa">
            <Navbar />
            <Jasa />
          </Route>
          <Route exact path="/aboutus">
            <Navbar />
            <AboutUs />
          </Route>
          <Route path="/jasa/programtech">
            <Navbar />
            <JasaProg />
          </Route>
          <Route path="/jasa/graphicdesign">
            <Navbar />
            <JasaGraph />
          </Route>
          <Route path="/jasa/videoanimation">
            <Navbar />
            <JasaVideo />
          </Route>
          <Route path="/jasa/:id">
            <Navbar />
            <DetailJasa />
          </Route>
          <Route exact path="/createJasa">
            <NavbarDash />
            <CreateJasa />
          </Route>
          <Route exact path="/createProject">
            <NavbarDash />
            <CreateProject />
          </Route>
          <Route exact path="/project">
            <NavbarDash />
            <Project />
          </Route>
          <Route exact path="/posting">
            <NavbarDash />
            <Posting />
          </Route>
        </Switch>
      </div>
    </Router>
    
  );
}

export default App;
