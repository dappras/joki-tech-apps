import { Link } from "react-router-dom";
import React from 'react'
import '../asset/CSS/NavbarDash.css'

const NavbarDash = () => {
    return (
        <div className="navbarDash">
            <div className="bagianKiri">
                <Link to="/">
                    <div className="kiri">    
                        <img src={ process.env.PUBLIC_URL + "/logoWeb.png" }/>
                        <h6>JokiTech</h6>
                    </div>    
                </Link>
            </div>

            <div className="bagianTengah">
                <Link to="/">Home</Link>
                <Link to="/project">Project</Link>
                <Link to="/posting">Posting</Link>
                <Link to="#">Account</Link>
            </div>

            <div className="bagianKanan">
                <h4>Username</h4>
                <img src={ process.env.PUBLIC_URL + "/profilePict.jpg" } />
            </div>
        </div>
    )
}

export default NavbarDash
