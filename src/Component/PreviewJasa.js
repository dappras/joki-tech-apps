import { Link } from "react-router-dom";
import '../asset/CSS/PreviewJasa.css'

const PreviewJasa = (props) => {
    const data = props.data

    return (  
        <div className="preview-jasa">
            {data.map((isi)=> (
                <Link to={"/jasa/"+isi.id}>
                    <div className="preview">
                        <h3>{ isi.judul }</h3>
                        <h4>Starting Price: { isi.startingPrice }</h4>
                        <h5>Penjasa: { isi.penjasa }</h5>
                    </div>
                </Link>
                
            ))}
        </div>
    );
}
 
export default PreviewJasa;