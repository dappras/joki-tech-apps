import React from 'react'
import { Link } from "react-router-dom";
import { BiPencil, BiTrash } from "react-icons/bi";
import '../asset/CSS/ListJasaPost.css';

const ListJasaPost = (props) => {
    const data = props.data

    const deletePost = (id) => {
        fetch('http://localhost:8000/jasa/' + id, {
            method: "DELETE"
        }).then(() => {
            window.location.reload();
        })
    }

    return (
        <div className="JasaPenjasa">
            {data.map((isi) => (
                <div>
                    <Link to={"jasa/" + isi.id}>
                        <div className="preview">
                            <h3>{isi.judul}</h3>
                            <div className="kotak">

                                <div className="deskripsi">
                                    <h3>Deskripsi</h3>
                                    <p>{isi.deskripsi.substring(0, 230)}...</p>
                                </div>

                                <div className="startingPrice">
                                    <h3>Starting Price</h3>
                                    <p>{isi.startingPrice}</p>
                                </div>

                            </div>   
                        </div>
                    </Link>
                    <div className="kotakBawah">
                        <BiPencil size="20px" color="white" />
                        <button onClick={() => {deletePost(isi.id)}}>
                            <BiTrash size="20px" color="white" />
                        </button>
                    </div>
                </div>
            ))}
        </div>
    )
}

export default ListJasaPost
