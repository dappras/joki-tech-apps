import { Link } from "react-router-dom";
import React from "react";
import '../asset/CSS/Navbar.css'

const Navbar = () => {

    return ( 
        <div className="navbar">
            <div className="bagian-kiri">
                <Link to="/">
                    <div className="kiri">    
                        <img src={ process.env.PUBLIC_URL + "/logoWeb.png" }/>
                        <h6>JokiTech</h6>
                    </div>    
                </Link>
                
                <div className="kanan">
                    <Link to="/">Home</Link>
                    <Link to="/jasa">Jasa</Link>
                    <Link to="/aboutUs">About Us</Link>
                </div>
            </div>
            <Link to="/project">
                <div className="bagian-kanan">
                    <p>Dashboard</p>
                </div>
            </Link>
            
        </div>
    );
}
 
export default Navbar;