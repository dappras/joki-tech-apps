import { Link } from "react-router-dom";
import '../../asset/CSS/Home.css'

const Home = () => {
    return ( 
        <div className="home">
            <div className="cover">
                <div className="bagian-kiri">
                    <img src={ process.env.PUBLIC_URL + "/logoCover.png" }/>
                </div>
                <div className="bagian-kanan">
                    <h1>Find The Perfect Service For Your Business</h1>
                    <p>JokiTech merupakan sebuah platform bagi penjasa untuk</p>
                    <p>menawarkan layanan mereka kepada pelanggan</p>
                    <p>di seluruh dunia.</p>
                </div>
            </div>
            
            <div className="konten">
                <h2>Our Service</h2>
                <hr />

                <div className="kategori">
                    <Link to="/jasa/programtech">
                        <div className="jasa">
                            <img src={ process.env.PUBLIC_URL + "/iconKat1.png" } />
                            <h3>Programming And Tech</h3>
                            <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Molestias error harum minus dolores iste quo, porro optio veniam inventore molestiae.</p>
                        </div>
                    </Link>
                    <Link to="/jasa/graphicdesign">
                        <div className="jasa">
                            <img src={ process.env.PUBLIC_URL + "/iconKat2.png" } />
                            <h3>Graphic and Design</h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Odit dolores obcaecati neque quas quia. Eligendi et dolorem deserunt sit dolore!</p>
                        </div>
                    </Link>
                    <Link to="/jasa/videoanimation">
                        <div className="jasa">
                            <img src={ process.env.PUBLIC_URL + "/iconKat3.png" } />
                            <h3>Video and Animation</h3>
                            <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Provident reprehenderit, ut delectus debitis enim ipsam dolore dignissimos magnam illo voluptatum?</p>
                        </div>
                    </Link>
                </div>
            </div>
        </div>
    );
}
 
export default Home;