import React from 'react'
import { useState } from 'react';
import '../../asset/CSS/CreateProject.css'
import { useHistory } from "react-router";

const CreateProject = () => {
    const [noPesanan, setNoPesanan] = useState("")
    const [kategori, setKategori] = useState("Programming and Tech")
    const [pemesanan, setPemesanan] = useState("")
    const [biaya, setBiaya] = useState(0)
    const status = 0
    const history = useHistory()

    const adding = (e) => {
        const data = {noPesanan, kategori, pemesanan, biaya, status}
        e.preventDefault()

        fetch("http://localhost:8000/project", {
            method: "POST",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify(data)
        }).then(() => {
            history.push("/project")
        })
    }

    return (
        <div className="create-projek">
            <h2>Create Project</h2>
            <form onSubmit={adding}>
                <label>Nomor Pesanan</label>
                <input 
                    type="text"
                    required
                    value = {noPesanan}
                    onChange = {(e) => setNoPesanan(e.target.value)}
                />
                <label>Kategori</label>
                <select
                    value = {kategori}
                    onChange = {(e) => setKategori(e.target.value)}
                >
                    <option value="Programming and Tech">Programming and Tech</option>
                    <option value="Graphic and Design">Graphic and Design</option>
                    <option value="Video and Animation">Video and Animation</option>
                </select>
                <label>Pemesanan</label>
                <input 
                    type="text"
                    required
                    value = {pemesanan}
                    onChange = {(e) => setPemesanan(e.target.value)}
                />
                <label>Biaya</label>
                <input 
                    type="number"
                    required
                    value = {biaya}
                    onChange = {(e) => setBiaya(e.target.value)}
                />

                <button>Add Project</button>
            </form>
        </div>
    )
}

export default CreateProject
