import React from 'react'
import { BiSearchAlt } from "react-icons/bi";
import { AiOutlineAppstoreAdd } from "react-icons/ai";
import useFetch from '../../Component/UseFetch'
import ListJasaPost from '../../Component/ListJasaPost';
import '../../asset/CSS/Posting.css'
import { Link } from "react-router-dom";

const Posting = () => {
    const {data, load} = useFetch("http://localhost:8000/jasa")

    return (
        <div className="posting">
            <div className="search">
                <BiSearchAlt size='14px' color="#772525" />
                <input type="text" placeholder="Search Posting..." />
            </div>

            <Link to="/createjasa">
                <div className="tambah">
                    <AiOutlineAppstoreAdd size="18px" color="white" />
                    <p>Tambah</p>
                </div>
            </Link>

            <div className="listJasa">
                {load && <h4>LOADING</h4>}

                {data && <ListJasaPost data={data} />}
            </div>
        </div>
    )
}

export default Posting
