import React, { useEffect } from 'react'
import '../../asset/CSS/Project.css'
import useFetch from '../../Component/UseFetch'
import { BiMoney, BiSearchAlt } from "react-icons/bi";
import { BsFillPlusCircleFill } from "react-icons/bs";
import { FaBookOpen, FaCheckCircle } from "react-icons/fa";
import axios from 'axios';
import { useState } from 'react';
import { Link } from "react-router-dom";

const Project = () => {
    const {data, load} = useFetch("http://localhost:8000/project")
    const [checked1, setChecked1] = useState(false)
    const [checked2, setChecked2] = useState(true)
    const [id, setId] = useState(0)
    const [noPesanan, setNoPesanan] = useState("")
    const [kategori, setKategori] = useState("")
    const [pemesanan, setPemesanan] = useState("")
    const [biaya, setBiaya] = useState(0)

    const dataNow = (data) => {
        for(const item of data){
            if(item.id===id){
                setNoPesanan(item.noPesanan)
                setKategori(item.kategori)
                setPemesanan(item.pemesanan)
                setBiaya(item.biaya)
            }
        }
    }

    useEffect(() => {
        data && dataNow(data)
        if (checked1==true){
            axios.put("http://localhost:8000/project/" + id,
                {
                    noPesanan: noPesanan,
                    kategori: kategori,
                    pemesanan: pemesanan,
                    biaya: biaya,
                    status: "1"
                }
            ).then(() => {
                window.location.reload();
            })
        } else {
            axios.put("http://localhost:8000/project/" + id,
                {
                    noPesanan: noPesanan,
                    kategori: kategori,
                    pemesanan: pemesanan,
                    biaya: biaya,
                    status: "0"
                }
            ).then(() => {
                window.location.reload();
            })
        }
    })

    const pendapatan = (data) => {
        let total = 0

        data.map((isi) => (
            total += parseInt(isi.biaya)
        ))

        return total
    }

    const projek = (data) => {
        let total = 0

        data.map((isi) => (
            total += 1
        ))

        return total
    }
    
    const finished = (data) => {
        let total = 0

        data.map((isi) => (
            total += parseInt(isi.status)
        ))

        return total
    }

    return (
        <div className="projek">
            <div className="bagianKiri">
                <div className="pendapatan">
                    <BiMoney size='25px' color='white' />
                    <div className="konten">
                        <h2>Pendapatan</h2>
                        {data && <h3>{pendapatan(data)}</h3>}
                    </div>
                </div>

                <div className="project">
                    <FaBookOpen size='25px' color='white' />
                    <div className="konten">
                        <h2>Project</h2>
                        {data && <h3>{projek(data)}</h3>}
                    </div>
                </div>

                <div className="finish">
                    <FaCheckCircle size='25px' color='white' />
                    <div className="konten">
                        <h2>Finish</h2>
                        {data &&  <h3>{finished(data)}</h3>}
                    </div>
                </div>
            </div>

            <div className="bagianKanan">
                <div className="window">
                    <div className="atas">
                        <h3>Project</h3>
                        <div className="search">
                            <BiSearchAlt size='14px' color="#772525" />
                            <input type="text" />
                        </div>
                        <Link to="/createProject">
                            <BsFillPlusCircleFill size='30px' color="#772525" />
                        </Link>
                    </div>
                    <div className="tengah">
                        <p>No Pesanan</p>
                        <p>Kategori</p>
                        <p>Pemesanan</p>
                        <p>Biaya</p>
                        <p>Status</p>
                    </div>
                    <div className="bawah">
                        {load && <h4>LOADING</h4>}

                        {data && 
                            data.map((isi)=>(
                                <div className="list">
                                    <p>{isi.noPesanan}</p>
                                    <p>{isi.kategori}</p>
                                    <p>{isi.pemesanan}</p>
                                    <p>{isi.biaya}</p>
                                    {isi.status==0 && 
                                        <input 
                                            type="checkbox" 
                                            defaultChecked={checked1}
                                            onChange = {() => {
                                                setChecked1(!checked1)
                                                setId(isi.id)
                                            }}
                                        />
                                    }
                                    {isi.status==1 && 
                                        <input 
                                            type="checkbox" 
                                            defaultChecked={checked2}
                                            onChange = {() => {
                                                setChecked1(!checked2)
                                                setId(isi.id)
                                            }}
                                        />
                                    }
                                </div>
                            ))
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Project
