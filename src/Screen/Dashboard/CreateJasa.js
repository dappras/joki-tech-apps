import { useHistory } from "react-router";
import { useState } from "react";
import '../../asset/CSS/CreateJasa.css'

const CreateJasa = () => {
    const [judul, setJudul] = useState('')
    const [penjasa, setPenjasa] = useState('')
    const [startingPrice, setStartingPrice] = useState('')
    const [deskripsi, setDeskripsi] = useState('')
    const [kategori, setKategori] = useState('Programming and Tech')
    const history = useHistory()
    
    const adding = (e) => {
        const data = {judul, deskripsi, penjasa, kategori, startingPrice}
        e.preventDefault()

        fetch("http://localhost:8000/jasa", {
            method: "POST",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify(data)
        }).then(() => {
            history.push("/posting")
        })
    }

    return (  
        <div className="create-jasa">
            <h2>Create Jasa</h2>
            <form onSubmit={adding}>
                <label>Judul Jasa</label>
                <input 
                    type="text" 
                    required
                    value = {judul}
                    onChange = {(e) => setJudul(e.target.value)}
                />
                <label>Penjasa</label>
                <input 
                    type="text" 
                    required
                    value = {penjasa}
                    onChange = {(e) => setPenjasa(e.target.value)}
                />
                <label>Starting Price</label>
                <input 
                    type="text" 
                    required
                    value = {startingPrice}
                    onChange = {(e) => setStartingPrice(e.target.value)}
                />
                <label>Deskripsi</label>
                <textarea 
                    required
                    value = {deskripsi}
                    onChange = {(e) => setDeskripsi(e.target.value)}
                ></textarea>
                <label>Kategori</label>
                <select
                    value = {kategori}
                    onChange = {(e) => setKategori(e.target.value)}    
                >
                    <option value="Programming and Tech">Programming and Tech</option>
                    <option value="Graphic and Design">Graphic and Design</option>
                    <option value="Video and Animation">Video and Animation</option>
                </select>

                <button>Add Jasa</button>
            </form>
        </div>
    );
}
 
export default CreateJasa;