import { Link } from "react-router-dom";
import PreviewJasa from "../../Component/PreviewJasa";
import useFetch from "../../Component/UseFetch";

const JasaVideo = () => {
    const {data, load} = useFetch("http://localhost:8000/jasa")

    return (  
        <div className="jasa-prog">
            <div className="link">
                <Link to="/">Home</Link>
                <img src={ process.env.PUBLIC_URL + "/arrow.png" }/>
                <Link to="/jasa">Jasa</Link>
                <img src={ process.env.PUBLIC_URL + "/arrow.png" }/>
                <Link to="/jasa/videoanimation">Video and Animation</Link>
            </div>
            
            <div className="konten">
                {load && <h4>LOADING</h4>}

                {data && <PreviewJasa data={data.filter((isi) => (
                    isi.kategori==="Video and Animation"
                ))}  />}
            </div>
        </div>
    );
}
 
export default JasaVideo;
