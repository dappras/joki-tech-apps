import { Link } from "react-router-dom";
import PreviewJasa from "../../Component/PreviewJasa";
import useFetch from "../../Component/UseFetch";
import '../../asset/CSS/JasaProg.css'

const JasaProg = () => {
    const {data, load} = useFetch("http://localhost:8000/jasa")

    return ( 
        <div className="jasa-prog">
            <div className="link">
                <Link to="/">Home</Link>
                <img src={ process.env.PUBLIC_URL + "/arrow.png" }/>
                <Link to="/jasa">Jasa</Link>
                <img src={ process.env.PUBLIC_URL + "/arrow.png" }/>
                <Link to="/jasa/programtech">Programming and Tech</Link>
            </div>
            
            <div className="konten">
                {load && <h4>LOADING</h4>}

                {data && <PreviewJasa data={data.filter((isi) => (
                    isi.kategori==="Programming and Tech"
                ))}  />}
            </div>
        </div>
    );
}
 
export default JasaProg;