import { useParams } from "react-router";
import { Link } from "react-router-dom";
import useFetch from "../../Component/UseFetch";
import '../../asset/CSS/DetailJasa.css'

const DetailJasa = () => {
    const {id} = useParams()
    const {data, load} = useFetch("http://localhost:8000/jasa/"+id)

    const kategori = (kat) => {
        if (kat==="Programming and Tech"){
            return "programtech"
        } else if(kat==="Graphic and Design"){
            return "graphicdesign"
        } else if(kat==="Video and Animation"){
            return "videoanimation"
        }
    }

    return (  
        <div className="detail-jasa">
            {load && <h4>LOADING...</h4>}

            {data && (
                <div className="pembungkus">
                    <div className="link">
                        <Link to="/">Home</Link>
                        <img src={ process.env.PUBLIC_URL + "/arrow.png" }/>
                        <Link to="/jasa">Jasa</Link>
                        <img src={ process.env.PUBLIC_URL + "/arrow.png" }/>
                        <Link to={"/jasa/" + kategori(data.kategori)} >{ data.kategori }</Link>
                        <img src={ process.env.PUBLIC_URL + "/arrow.png" }/>
                        <Link to={"/jasa/" + data.id} >{ data.judul }</Link>
                    </div>
                    <article>
                        <div className="kiri">

                        </div>
                        <div className="tengah">
                            <h2>{data.judul}</h2>
                            <p>{data.deskripsi}</p>
                        </div>
                        <div className="kanan">
                            <div className="atas">
                                <h3>Starting Price</h3>
                                <h4>{data.startingPrice}</h4>
                            </div>
                            <div className="bawah">
                                <h3>Contact Person</h3>
                            </div>
                        </div>
                    </article>
                </div>
                
            )}
        </div>
    );
}
 
export default DetailJasa;