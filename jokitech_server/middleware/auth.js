const expressJwt = require('express-jwt');

function authJwt() {
  const api = process.env.API_URL;

  const secret = process.env.secret;
  return expressJwt({
    secret,
    algorithms: ['HS256']
  }).unless({
    path: [
      {url: `${api}/jasas`, methods: ['GET', 'OPTIONS']},
      {url: `${api}/jasas/1`, methods: ['GET', 'OPTIONS']},
      {url: `${api}/jasas/2`, methods: ['GET', 'OPTIONS']},
      {url: `${api}/jasas/3`, methods: ['GET', 'OPTIONS']},
      {url: '/\/public/uploads(.*)/', methods: ['GET', 'OPTIONS']},
      '/api/v1/penjasas/login',
      '/api/v1/penjasas/register'
    ]
  })
};

module.exports = authJwt;
