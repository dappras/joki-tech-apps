const mongoose = require('mongoose');

function defaultProfilePath (req) {
  const basePath = `${req.protocol}://${req.get('host')}/public/upload/default-profile.jpg`;
  return basePath;
}

const penjasaSchema = mongoose.Schema({
  email: {
    type: String
  },
  password: {
    type: String
  },
  namaPenjasa : {
    type: String
  },
  username: {
    type: String
  },
  nomorHpPenjasa: String,
  aboutMe: String,
  profilePicture: String
});

penjasaSchema.virtual('id').get(function () {
  return this._id.toHexString();
});

penjasaSchema.set('toJSON', {
  virtuals: true
})

exports.Penjasa  = mongoose.model('Penjasa', penjasaSchema);
exports.penjasaSchema = penjasaSchema;
