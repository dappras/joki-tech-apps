const mongoose = require('mongoose');

// 1 Graphic and Design
// 2 Programming and Tech
// 3 Video and Animation

const historyProjectSchema = mongoose.Schema({
  penjasaId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Penjasa'
  },
  namaPencariJasa: {
    type: String,
    required: true
  },
  kategoriPenjasa: {
    type: String,
    required: true
  },
  totalBiaya: {
    type: Number,
    required: true
  },
  statusProject: {
    type: String,
    required: true
  }
});

historyProjectSchema.virtual('id_Penasan').get(function () {
  return this._id.toHexString();
});

historyProjectSchema.set('toJSON', {
  virtuals: true
})

exports.HistoryProject  = mongoose.model('HistoryProject', historyProjectSchema);
