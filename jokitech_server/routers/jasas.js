const {Jasa} = require('../models/jasa');
const express = require('express');
const router = express.Router();
const mongoose = require("mongoose");
const multer = require('multer');

const FILE_TYPE_MAP = {
  'image/png': 'png',
  'image/jpeg': 'jpeg',
  'image/jpg': 'jpg'
};

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    const isValid = FILE_TYPE_MAP[file.mimetype];
    let uploadError = new Error('invalid image type');

    if(isValid) {
      uploadError = null;
    }
    cb(uploadError, 'public/uploads');
  },
  filename: function (req, file, cb) {
    const fileName = file.originalname.split(' ').join('-');
    const extension = FILE_TYPE_MAP[file.mimetype];
    cb(null, `${fileName}-${Date.now()}.${extension}`);
  }
})

const upload = multer({ storage: storage })

/************** GET **************/

router.get(`/`, async (req, res)=> {
  const jasaList = await Jasa.find();

  if(!jasaList) {
    res.status(500).json({
      success: false
    })
  }
  res.send(jasaList);
});

// 1 Graphic and Design
router.get(`/1`, async (req, res)=> {
  const jasaList1 = await Jasa.find({catJasa: 1});

  if(!jasaList1) {
    res.status(500).json({
      success: false
    })
  }
  res.send(jasaList1);
});

// 2 Programming and Tech
router.get(`/2`, async (req, res)=> {
  const jasaList2 = await Jasa.find({catJasa: 2});

  if(!jasaList2) {
    res.status(500).json({
      success: false
    })
  }
  res.send(jasaList2);
});

// 3 Video and Animation
router.get(`/3`, async (req, res)=> {
  const jasaList3 = await Jasa.find({catJasa: 3});

  if(!jasaList3) {
    res.status(500).json({
      success: false
    })
  }
  res.send(jasaList3);
});

router.get('/:id', async (req, res)=>{
  const jasa = await Jasa.findById(req.params.id);

  if(!jasa) {
    res.status(500).json({message: 'Jasa with following id not found'});
  }
  res.status(200).send(jasa);
});


/************** POST **************/

router.post(`/createJasa`, upload.single('image'), async (req, res)=> {
  const file = req.file;
  if(!file) return res.status(400).send('No image in the request');

  const fileName = req.file.filename;
  const basePath = `${req.protocol}://${req.get('host')}/public/uploads/`;
  
  let jasa = new Jasa({
    namaJasa: req.body.namaJasa,
    penjasaId: req.body.penjasaId,
    penjasaHpNo: req.body.penjasaHpNo,
    penjasaEmail: req.body.penjasaEmail,
    startingPrice: req.body.startingPrice,
    descJasa: req.body.descJasa,
    catJasa: req.body.catJasa,
    image: `${basePath}${fileName}`
  });

  jasa = await jasa.save();

  if(!jasa) {
    return res.status(404).send('the jasa cannot be created');
  }

  res.send(jasa);

});

/************** DELETE **************/

router.delete('/deleteJasa/:id', async (req, res)=> {
  Jasa.findByIdAndDelete(req.params.id)
    .then(jasa => {
      if(jasa) {
        return res.status(200).json({success: true, message: 'Jasa is deleted'});
      } else {
        return res.status(404).json({success: false, message: 'Jasa not found'});
      }
    }).catch(err=>{
      return res.status(400).json({success: false, error: err});
    })
});


module.exports = router;

/************** UPDATE **************/

router.put('/updateJasa/:id', async (req, res) => {
  const jasa = await Jasa.findOneAndUpdate(
  req.params.id,
    {
      namaJasa: req.body.namaJasa,
      penjasaId: req.body.penjasaId,
      penjasaHpNo: req.body.penjasaHpNo,
      penjasaEmail: req.body.penjasaEmail,
      startingPrice: req.body.startingPrice,
      descJasa: req.body.descJasa,
      catJasa: req.body.catJasa
    },
  { new: true }
  )

  if(!jasa) {
    return res.status(404).send('the jasa cannot be created');
  }

  res.send(jasa);
});
